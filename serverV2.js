var express = require('express');
var bodyParser = require('body-parser');
var requestJson = require('request-json')
var app = express();
var port = process.env.PORT || 3000;
var usersFile = require('./users.json');
var URLbase = "/colapi/v3/";
var URLMlab= 'https://api.mlab.com/api/1/databases/colapidb_cfgc/collections/';
var apiKey='apiKey=uknXNd4Yb5qIYsbmNQpFi44Z5LFTxmkm';

app.listen(port, function(){
 console.log("API apicol escuchando en el puerto " + port + "...");
});

app.use(bodyParser.json());

// GET users
app.get(URLbase + 'users',
 function(request, response) {
   var httpClient= requestJson.createClient(URLMlab);
   var queryString='f={_id:0}&';
   console.log('cliente http Mlab creado');
   httpClient.get('user?' + queryString + apiKey,
    function(err,respuestaMLab,body){
      console.log('error' + err);
      console.log('respuesta' + respuestaMLab);
      console.log('body'+ body);
      var respuesta={};
      respuesta =!err ? body: {"msg error al retornar de MLab":err};
      response.send(respuesta);
    });
   console.log('get colapi/v3/');
//   response.send({"msg":"get OK MLAB"});
});


// Petición GET con parámetros (req.params)
app.get(URLbase + 'users/:id',
 function (req, res) {
   console.log("GET /colapi/v2/users/:id");
   console.log(req.params.id);
   var id = req.params.id;
   var httpClient= requestJson.createClient(URLMlab);
   var queryString='f={"_id":0}&q={"id":' + id + '}&';
   console.log('cliente http Mlab creado');
   httpClient.get('user?' + queryString + apiKey,
    function(err,respuestaMLab,body){
      console.log('error' + err);
      console.log('respuesta' + respuestaMLab);
      console.log('body'+ body);
      var respuesta={};
      respuesta =!err ? body[0]: {"msg error al retornar de MLab":"err"};
      res.send(respuesta);
    });
   console.log('get colapi/v3/');
});

app.post(URLbase + 'users', function(req, res) {
clienteMlab = requestJson.createClient(URLMlab + '/user?' + apiKey);
clienteMlab.post('', req.body,
function(err, resM, body) {
  res.send(body);
})
});

app.put(URLbase + 'users/:id', function(req, res) {
 clienteMlab = requestJson.createClient(URLMlab + "/user");
 var cambio = '{"$set":' + JSON.stringify(req.body) + '}';
 console.log(cambio);
 variableVer='?q={"id":' + req.params.id + '}&' + apiKey + JSON.parse(cambio);
 console.log('iniviover' + variableVer + 'termina');
 clienteMlab.put('?q={"id":' + req.params.id + '}&' + apiKey, JSON.parse(cambio), function(err, resM, body) {
   res.send(body);
   console.log(req.params.id);
   console.log(resM);
   console.log(body);
 })
});

// login con MLab adicionando campo $con
app.put(URLbase + 'login2/:id', function(req, res) {
 clienteMlab = requestJson.createClient(URLMlab + "/user");
 var cambio = '{"$set":' + JSON.stringify({"logued":true}) + '}';
 console.log(cambio);
  clienteMlab.put('?q={"id":' + req.params.id + '}&' + apiKey, JSON.parse(cambio), function(err, resM, body) {
   res.send(body);
 })
});


//logut borrando el campo con $unset parametro
app.post(URLbase + 'login4', function(req, res) {
 clienteMlab = requestJson.createClient(URLMlab + "/user");
 var cambio = '{"$set":' + JSON.stringify({"logued":true}) + '}';
 console.log(cambio);
 //console.log(req.params.id);
 clienteMlab.put('?q={"email":"' + req.body.email + '","password":"' + req.body.password  + '"}&' + apiKey, JSON.parse(cambio),
 function(err, resM, body) {
   //res.send(body);
      console.log('consolacfga' + body.n)
   if (body.n==0){
     res.send({'msg':'usuario o contraseña invalidos'});
  } else {
    res.send({'msg':'usuario logeado exitosamente!!'});
  }
 })
});

//logut borrando el campo con $unset body
app.post(URLbase + 'logout', function(req, res) {
 clienteMlab = requestJson.createClient(URLMlab + "/user");
 var cambio = '{"$unset":' + JSON.stringify({"logued":""}) + '}';
 console.log(cambio);
 console.log(req.params.id);
 clienteMlab.put('?q={"email":"' + req.body.email +  '"}&' + apiKey, JSON.parse(cambio),
 function(err, resM, body) {
   //res.send(body);
   if (body.n==0){
     console.log('longitudcfga' + body.length);
     res.send({'msg':'email no existe','e-mail': req.body.email});
  } else {
    res.send({'msg':'Salida Exitosa!! Hasta pronto!!'});
  }
 })
});

app.delete(URLbase + 'bajas', function(req,res){
  clienteMLab= requestJson.createClient(URLMlab + "/user")
});

// Petición GET con Query String (req.query)
app.get(URLbase + 'users',
 function(req, res) {
   console.log("GET con query string.");
   console.log(req.query.id);
   console.log(req.query.country);
   res.send(usersFile[pos - 1]);
   respuesta.send({"msg" : "GET con query string"});
});

// Petición POST (login)
app.post(URLbase + 'login3',
 function(req, res) {
   var email = req.body.email;
   var pass = req.body.pass;
   for (us of usersFile){
      if (us.email==email){
        if (us.password==pass){
          us.loggued=true;
          writeUserDataToFile(userFile);
          console.log(us);
          res.send({'login':'exitoso'});
          }else {
          res.send({'password':'incorrecta', 'user': email});
        }
      }
    }
    res.send({'e-mail':'no existe', 'email': email});
 });

 // Petición POST (logout)
 app.post(URLbase + 'logout',
  function(req, res) {
    var email = req.body.email;
    var pass = req.body.pass;
    for (us of usersFile){
       if (us.email==email){
         if (us.password==pass){
           delete us.loggued;
           writeUserDataToFile(usersFile);
           console.log(us);
           res.send({'logout':'exitoso'});
           }else {
           res.send({'password':'incorrecta', 'user': email});
           }
       }
     }
     res.send({'e-mail':'no existe', 'email': email});
  });

// peticion put

// PUT
app.put(URLbase + 'users/:id/:otro',
  function(req, res){
    console.log("PUT /colapi/v2/users/:id");
    var idBuscar = req.params.id;
    var updateUser = req.body;
    var encontrado = false;
    for(i = 0; (i < usersFile.length) && !encontrado; i++) {
      console.log(usersFile[i].id);
      if(usersFile[i].id == idBuscar) {
        encontrado = true;
        res.send({"msg" : "Usuario actualizado correctamente.", updateUser});
      }
    }
    if(!encontrado) {
      res.send({"msg" : "Usuario no encontrado.", updateUser});
    }
  });

// DELETE
app.delete(URLbase + 'users/:id',
 function(req, res) {

   const id = req.params.id-1;
   const reg = usersFile[id];

   if(undefined != reg){
     usersFile.splice(id,1);
     res.send(204);
  } else
    res.send(404);
});

// persistir en archivo en disco funcion write;
function writeUserDataToFile(usersFile) {
 var fs = require('fs');
 var jsonUserData = JSON.stringify(usersFile);

 fs.writeFile("./users.json", jsonUserData, "utf8",
  function(err) { //función manejadora para gestionar errores de escritura
    if(err) {
      console.log(err);
    } else {
      console.log("Datos escritos en 'users.json'.");
    }
  })
}

//GET users a traves de mLab con parametros
app.get(URLbase + 'users/:id/accounts',
 function(request, response){
     var idParam = request.params.id;
     var httpClient = requestJson.createClient(URLMlab);
     var queryString = 'q={"userid":' + idParam + '}&';
     httpClient.get('account?'+ queryString + apiKey,
         function(e, respuestaMLab, body){
             var answer = body;
             response.send(answer);
         });
});
