#imagen base para crear nuestra imagen
FROM node

# Dirextorio donde ejecutara
WORKDIR /colapi_cfgc

# añadimos el contenido del proyecto que esta en la carpeta del proyecto, con espacio despues del punto
ADD . /colapi_cfgc

#puerto de escucha del contenedor/ debe ser el mismo definido en la aplicaciòn
EXPOSE  3000

#Comando para lanzar nuestra api, para producciòn se debe cambiar a ("node","server2.js")
#CMD ("node","server2.js")
CMD ["npm","start"]
