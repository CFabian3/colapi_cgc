var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');
var server = require('../serverV2');

var should = chai.should();

chai.use(chaihttp) ;// configurar chai en modo const http = require('http')

describe('pruebas colombia', () => {
  it('test BBVA', (done) => {
    chai.request('https://www.bbva.com.co')
        .get('/')
        .end((err, res) => {
          //console.log(res)
          //console.log(err)
          res.should.have.status(200);
          done();
        })
  });
  it('test colapi', (done) => {
    chai.request('http://localhost:3000')
        .get('/colapi/v3/users')
        .end((err, res) => {
            //console.log(res)
            //console.log(err)
           res.should.have.status(200);
            done();
         })
  });
  it('test colapi usuario', (done) => {
    chai.request('http://localhost:3000')
        .get('/colapi/v3/users')
        .end((err, res) => {
            //console.log(res)
            //console.log(err)
           res.body.should.be.a('Array');
            done();
         })
  });
  it('test colapi numero usuario', (done) => {
    chai.request('http://localhost:3000')
        .get('/colapi/v3/users')
        .end((err, res) => {
            //console.log(res)
            //console.log(err)
           res.body.length.should.be.gte(1);
            done();
         })
  });
  it('test colapi propiedades', (done) => {
    chai.request('http://localhost:3000')
        .get('/colapi/v3/users')
        .end((err, res) => {
            console.log(res.body[0])
            //console.log(er r)
           res.body[0].should.have.property("first_name");
           res.body[0].should.have.property("last_name");
            done();
         })
  });
  it('test colapi post', (done) => {
    chai.request('http://localhost:3000')
        .post('/colapi/v3/users')
        .send({'first_name':'Duvan','last_name':'chaves','email':'jdcc1@hotmail.com','password':'12345678'})
        .end((err, res, body) => {
            console.log(res.body[0])
            //console.log(err)
//           res.body.n.should.be.equal('1');
           //res.body.should.have.property('last_name')
            done();
         })
  });

});
